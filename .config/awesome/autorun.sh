#!/usr/bin/env bash

function run {
  if ! pgrep $1 ;
  then
    $@&
  fi
}

run sun
run google-chrome
run gitkraken
run thunderbird
run xscreensaver -no-splash
run synergy
run jetbrains-toolbox

setxkbmap -layout "us,us_intl" -option "grp:alt_shift_toggle"
